#!/usr/bin/env node

/**
 * Module dependencies.
 */

// var app = require('../app');
var debug = require('debug')('match:server');
var http = require('http');
var socketio = require('socket.io');
var express = require('express');

var app = express();
/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

var server = http.createServer(app)
// var https = require('https');
// var fs    = require('fs');
// var httpOptions =  {

//   key: fs.readFileSync("e8472c1ac151d4c5.crt"),
//   key1: fs.readFileSync("e8472c1ac151d4c5.pem"),
//   cert: fs.readFileSync("gd_bundle-g2-g1.crt"),

//  key: fs.readFileSync("zserver.crt"),
//   key1: fs.readFileSync("zkey.pem")
//   cert: fs.readFileSync("zbundle.crt")

//   //  key: fs.readFileSync("private_key.key"),
//   // cert: fs.readFileSync("ceritificate.crt"),
//   passphrase: "pakistan123"

//  }
//  var server = https.createServer(httpOptions, app);

var io = socketio(server);
/**
 * Create HTTP server.
 */

// var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    var port = parseInt(val, 10);

    if (isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    var bind = typeof port === 'string' ?
        'Pipe ' + port :
        'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    var addr = server.address();
    var bind = typeof addr === 'string' ?
        'pipe ' + addr :
        'port ' + addr.port;
    debug('Listening on ' + bind);
}
























var createError = require('http-errors');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var cors = require('cors')
var fs = require('fs');

var pool = require('./db')


var block_userRouter = require('./routes/block_user')
var blockRouter = require('./routes/block')
var create_profileRouter = require('./routes/create_profile')
var discover_profileRouter = require('./routes/discover_profile')
var discover_searchRouter = require('./routes/discover_search')
var discoverRouter = require('./routes/discover')
var edit_personal_profileRouter = require('./routes/edit_personal_profile')
var edit_profileRouter = require('./routes/edit_profile')
var loginRouter = require('./routes/login')
var logoutRouter = require('./routes/logout')
var profileRouter = require('./routes/profile')
var signupRouter = require('./routes/signup')
var unblockRouter = require('./routes/unblock')
var upload_photoRouter = require('./routes/upload_photo')
var user_picRouter = require('./routes/user_pic')
var upload_profile_photoRouter = require('./routes/upload_profile_photo')
var chat_imageRouter = require('./routes/chat_image')
var unread_conversationsRouter = require('./routes/unread_conversations')




// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

app.use('/block_user', block_userRouter);
app.use('/block', blockRouter);
app.use('/create_profile', create_profileRouter);
app.use('/discover_profile', discover_profileRouter);
app.use('/discover_search', discover_searchRouter);
app.use('/discover', discoverRouter);
app.use('/edit_personal_profile', edit_personal_profileRouter);
app.use('/edit_profile', edit_profileRouter);
app.use('/login', loginRouter);
app.use('/logout', logoutRouter);
app.use('/profile', profileRouter);
app.use('/signup', signupRouter);
app.use('/unblock', unblockRouter);
app.use('/upload_photo', upload_photoRouter);
app.use('/user_pic', user_picRouter);
app.use('/upload_profile_photo', upload_profile_photoRouter);
app.use('/chat_image', chat_imageRouter);
app.use('/unread_conversations', unread_conversationsRouter)





const rooms = {}

app.get('/', function (req, res, next) {
    res.send("testing");
});
// When user click on chat button
app.get('/chats', (req, res) => {
    var user_id = req.query.user_1;
    var arr = [];
    var arr2 = [];
    var ind = 0;
    pool.query("SELECT user_1,user_2,room_id FROM rooms WHERE user_1='" + user_id + "' OR user_2='" + user_id + "' ").then(async rows => {
        var leng = rows.length;
        console.log("rows =" + leng);
        var lenn = leng - 1
        if (rows.length >= 1) {
            console.log("length of side bar= " + leng)
            var i;
            for (i = lenn; i >= 0; i--) {
                console.log("i new=" + i);
                console.log("before");
                console.log("user " + rows[i].user_1);

                var block_users = await pool.query("select * from blocks where  (blocked='" + rows[i].user_1 + "' AND blocker='" + rows[i].user_2 + "') OR (blocker='" + rows[i].user_1 + "' AND blocked='" + rows[i].user_2 + "') ");
                if (block_users.length == 1) {
                    console.log(" blocked blocked blocked blocked ");
                } else {
                    console.log("in for " + rows[i].user_1);
                    if (rows[i].user_1 == req.query.user_1) {
                        console.log("in for in if " + rows[i].user_2);
                        const rows2 = await pool.query("select username,id,profile_photo_path from users where id='" + rows[i].user_2 + "'");
                        const result1 = await pool.query("select message,room_id from messages where (sender_id='" + rows[i].user_2 + "' OR reciever_id='" + rows[i].user_2 + "') AND (sender_id='" + rows[i].user_1 + "' OR reciever_id='" + rows[i].user_1 + "'  )");
                        var len = result1.length;
                        if (len != 0) {
                            len = len - 1;
                            console.log("Last Message " + result1[len].message);
                            rows2[0].last_message = result1[len].message;
                            if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
                                console.log("image not exist");
                            } else {
                                const path = '/compressed/' + rows2[0].profile_photo_path;
                                rows2[0].profile_photo = path;
                            }
                            pool.query("SELECT id FROM messages WHERE reciever_id='" + req.query.user_1 + "' AND status=0 AND room_id='" + result1[len].room_id + "'").then(total_chats => {
                                console.log("total conversations =" + total_chats.length);
                                rows2[0].unread_messages = total_chats.length;
                                arr.push(rows2[0]);

                            }).catch(err => {
                                console.log(err);

                            })
                        }
                    } else {
                        console.log("hi");
                        console.log("in for in else " + rows[i].user_1);
                        const rows3 = await pool.query("select username,id,profile_photo_path from users where id='" + rows[i].user_1 + "'");
                        const result2 = await pool.query("select message,room_id from messages where (sender_id='" + rows[i].user_2 + "' OR reciever_id='" + rows[i].user_2 + "') AND (sender_id='" + rows[i].user_1 + "' OR reciever_id='" + rows[i].user_1 + "'  )");
                        var len = result2.length;
                        console.log("length " + len);
                        if (len != 0) {
                            console.log("in if for last massages");
                            len = len - 1;
                            console.log("Last Message in else " + result2[len].message);
                            rows3[0].last_message = result2[len].message;
                            console.log("LAST MESSAGE = " + rows3[0].last_message);
                            if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
                                console.log("image not exist");
                            } else {
                                const path = '/compressed/' + rows3[0].profile_photo_path;
                                rows3[0].profile_photo = path;
                            }
                            pool.query("SELECT id FROM messages WHERE reciever_id='" + req.query.user_1 + "' AND status=0 AND room_id='" + result2[len].room_id + "'").then(total_chats => {
                                console.log("total conversations =" + total_chats.length);
                                rows3[0].unread_messages = total_chats.length;
                                arr.push(rows3[0]);

                            }).catch(err => {
                                console.log(err);

                            })


                        }




                    }

                }

            }
            pool.query("select room_id,sender_id,reciever_id from messages where sender_id='" + req.query.user_1 + "' or reciever_id='" + req.query.user_1 + "'").then(async rows4 => {
                console.log("In mesage query");
                if (rows4.length >= 1) {
                    var i = 0;
                    var len = rows4.length;
                    var ind = 0
                    len = len - 1;

                    for (i = len; i >= 0; i--) {
                        console.log("i= " + i);

                        var block_users = await pool.query("select * from blocks where  (blocked='" + rows4[i].sender_id + "' AND blocker='" + rows4[i].reciever_id + "') OR (blocker='" + rows4[i].sender_id + "' AND blocked='" + rows4[i].reciever_id + "') ")
                        if (block_users.length == 1) {
                            console.log("blocked blocked id=" + rows4[i].sender_id + " or id=" + rows4[i].reciever_id);

                        } else {
                            var room_id = rows4[i].room_id;
                            console.log("selected room_id = " + room_id);
                            console.log("Room ID = " + rows4[i].room_id);
                            rooms[rows4[i].room_id] = {
                                users: {}
                            }
                            ind = i;
                            console.log("selected index= " + ind);
                            break;
                        }
                    }
                    console.log("selected index= " + ind);


                    if (rows4[ind].sender_id == req.query.user_1) {
                        pool.query("select username,year,location,country,profile_photo_path from users where id='" + rows4[ind].reciever_id + "'").then(rows5 => {
                            const dt = new Date();
                            var current_year = dt.getFullYear();
                            rows5[0].age = current_year - rows5[0].year;
                            if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
                                console.log("image not exist");
                            } else {
                                const path = '/compressed/' + rows5[0].profile_photo_path;
                                rows5[0].profile_photo = path;
                            }

                            pool.query("SELECT * FROM messages WHERE room_id='" + room_id + "'").then(rows6 => {

                                res.contentType('text/plain');

                                res.send({
                                    roomName: room_id,
                                    userID: req.query.user_1,
                                    data: rows6,
                                    reciever_data: rows5,
                                    sidebar_data: arr
                                });

                            }).catch(err => {
                                console.log("Error while retrieving messages");

                            })


                        }).catch(err => {
                            console.log("error while retrieing reciever data in if statement " + err);

                        })

                    } else {
                        pool.query("select username,year,location,country,profile_photo_path from users where id='" + rows4[len].sender_id + "'").then(rows5 => {
                            const dt = new Date();
                            var current_year = dt.getFullYear();
                            rows5[0].age = current_year - rows5[0].year;
                            if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
                                console.log("image not exist");
                            } else {
                                const path = '/compressed/' + rows5[0].profile_photo_path;
                                rows5[0].profile_photo = path;
                            }

                            pool.query("SELECT * FROM messages WHERE room_id='" + room_id + "'").then(rows6 => {

                                res.contentType('text/plain');

                                res.send({
                                    roomName: room_id,
                                    userID: req.query.user_1,
                                    data: rows6,
                                    reciever_data: rows5,
                                    sidebar_data: arr
                                });

                            }).catch(err => {
                                console.log("Error while retrieving messages");

                            })


                        }).catch(err => {
                            console.log("error while retrieing reciever data in else statement " + err);

                        })
                    }


                } else {
                    res.contentType('text/plain');
                    res.send("NO Chats Exist of yhis user")
                }


            }).catch(err => {
                console.log("Error while retrieving room_id from messages");

            })
        } else {
            res.contentType('text/plain');
            res.send("NO Chats Exist")
        }



        // res.render('chat', {
        //   roomName: req.query.room_id,
        //   userID:req.query.user_id,
        //   data:rows
        // })

        // conn.end();
        // console.log("Connection Closed");


    }).catch(err => {

    })

    // conn.end();


})


app.get('/create_room', (req, res) => {
    var user_1 = req.query.user_1;
    var user_2 = req.query.user_2;
    var room_id = 0;


    pool.query("select * from rooms where user_1='" + user_1 + "' AND user_2='" + user_2 + "' OR user_1='" + user_2 + "' AND user_2='" + user_1 + "'").then(rows => {
        if (rows.length >= 1) {
            rooms[rows[0].room_id] = {
                users: {}
            }
            var q = rows[0].room_id;
            console.log("room id = " + q);

            var sql = `UPDATE messages SET status = '1' WHERE reciever_id = '${req.query.user_1}' AND room_id='${q}'`
            pool.query(sql).then(status => {
                console.log("Status updated(All unread messages reaaded)");

            }).catch(err => {
                console.log(err);
            })

            res.redirect('msg?room_id=' + rows[0].room_id + '&user_id=' + user_1 + '&user_id2=' + user_2)
            // io.emit('room-created', rows[0].id)
        } else {
            pool.query("INSERT INTO rooms value(?,?,?)", [room_id, user_1, user_2]).then(rows => {
                console.log("new room created in data base");
                pool.query("select room_id from rooms where user_1= '" + user_1 + "' AND user_2= '" + user_2 + "'").then(rows => {
                    console.log("id " + rows[0].room_id);
                    rooms[rows[0].room_id] = {
                        users: {}
                    }
                    res.redirect('msg?room_id=' + rows[0].room_id + '&user_id=' + user_1 + '&user_id2=' + user_2)
                    // io.emit('room-created', rows[0].id)

                }).catch(err => {
                    console.log("err " + err);

                })

            }).catch(err => {
                console.log("err " + err);

            })
        }

    }).catch(err => {})


    // res.render('login', { rooms: rooms })
})

app.get('/msg', (req, res) => {
    var arr = [];
    // res.json({q:req.query.room_id})
    console.log("room_id: " + req.query.room_id);
    console.log("user_id: " + req.query.user_id);
    console.log("user_id2(reciever): " + req.query.user_id2);
    // console.log("user_email: " + req.params.user_email);

    pool.query("select * from messages where room_id='" + req.query.room_id + "'").then(rows => {
        pool.query("select username,year,location,country,profile_photo_path from users where id='" + req.query.user_id2 + "'").then(rows2 => {
            const dt = new Date();
            var current_year = dt.getFullYear();
            rows2[0].age = current_year - rows2[0].year;
            if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
                console.log("image not exist");
            } else {
                const path = '/compressed/' + rows2[0].profile_photo_path;
                rows2[0].profile_photo = path;
            }
            pool.query("select user_1,user_2 from rooms where user_1='" + req.query.user_id + "' or user_2='" + req.query.user_id + "' ").then(async rows3 => {
                var leng = rows3.length;
                console.log("length of side bar= " + leng)
                var i;
                for (i = 0; i < leng; i++) {
                    console.log("in for " + rows3[i].user_1);

                    if (rows3[i].user_1 == req.query.user_id) {
                        var block_users = await pool.query("select * from blocks where  (blocked='" + req.query.user_id + "' AND blocker='" + rows3[i].user_2 + "') OR (blocker='" + req.query.user_id + "' AND blocked='" + rows3[i].user_2 + "') ");
                        console.log("Blocks length= " + block_users.length);
                        if (block_users.length == 1) {
                            console.log(" blocked blocked blocked blocked ");
                        } else {
                            console.log("in for in if " + rows3[i].user_2);
                            const rows4 = await pool.query("select username,id,profile_photo_path from users where id='" + rows3[i].user_2 + "'");
                            const rows6 = await pool.query("select message from messages where (sender_id='" + rows3[i].user_2 + "' OR reciever_id='" + rows3[i].user_2 + "') AND (sender_id='" + rows3[i].user_1 + "' OR reciever_id='" + rows3[i].user_1 + "'  )");
                            var len = rows6.length;
                            if (len != 0) {
                                len = len - 1;
                                console.log("Last Message " + rows6[len].message);
                                rows4[0].last_message = rows6[len].message;
                                if (rows4[0].profile_photo_path == null || rows4[0].profile_photo_path == "") {
                                    console.log("image not exist");
                                } else {
                                    const path = '/compressed/' + rows4[0].profile_photo_path;
                                    rows4[0].profile_photo = path;
                                }
                                arr.push(rows4[0]);

                            }
                        }
                    } else {
                        var block_users = await pool.query("select * from blocks where  (blocked='" + req.query.user_id + "' AND blocker='" + rows3[i].user_1 + "') OR (blocker='" + req.query.user_id + "' AND blocked='" + rows3[i].user_1 + "') ");
                        console.log("Blocks length in else= " + block_users.length);

                        if (block_users.length == 1) {
                            console.log(" blocked blocked blocked blocked ");
                        } else {
                            console.log("in for in else " + rows3[i].user_1);

                            const rows5 = await pool.query("select username,id,profile_photo_path from users where id='" + rows3[i].user_1 + "'");
                            const rows7 = await pool.query("select message from messages where (sender_id='" + rows3[i].user_2 + "' OR reciever_id='" + rows3[i].user_2 + "') AND (sender_id='" + rows3[i].user_1 + "' OR reciever_id='" + rows3[i].user_1 + "'  )");
                            var len = rows7.length;
                            console.log("length " + len);
                            if (len != 0) {
                                console.log("in if for last massages");

                                len = len - 1;
                                console.log("Last Message in else " + rows7[len].message);
                                rows5[0].last_message = rows7[len].message;
                                console.log("LAST MESSAGE = " + rows5[0].last_message);
                                if (rows5[0].profile_photo_path == null || rows5[0].profile_photo_path == "") {
                                    console.log("image not exist");
                                } else {
                                    const path = '/compressed/' + rows5[0].profile_photo_path;
                                    rows5[0].profile_photo = path;
                                }
                                arr.push(rows5[0]);
                            }
                        }
                    }
                }
                res.contentType('text/plain');

                res.send({
                    roomName: req.query.room_id,
                    userID: req.query.user_id,
                    data: rows,
                    reciever_data: rows2,
                    sidebar_data: arr
                });
            }).catch(err => {})
        }).catch(err => {
            console.log("ERR= " + err);

        })

    }).catch(err => {})

})

io.on('connection', socket => {
    socket.on('new-user', (room, user_id) => {
        console.log("in connection room=" + room);
        console.log("in connection name=" + user_id);

        socket.join(room)

        rooms[room].users[socket.id] = user_id
        socket.to(room).broadcast.emit('user-connected', user_id)
    })
    socket.on('send-chat-message', (room, userID, message) => {
        console.log("server side chat mesage ");


        pool.query("select * from rooms where room_id='" + room + "'").then(rows => {
            console.log("reciever selected ");
            console.log("sender Id " + userID);
            var reciever_id;
            var id = 0;
            if (rows[0].user_1 != userID) {
                reciever_id = rows[0].user_1;
            } else {
                reciever_id = rows[0].user_2;
            }
            console.log("Reciever Id " + reciever_id);
            console.log("Message : " + message);
            console.log("UserID : " + userID);
            console.log("room : " + room);

            pool.query("INSERT INTO messages value(?,?,?,?,?,?,?)", [reciever_id, message, id, userID, room, 0, 0]).then(rows => {
                console.log("record inserted ");
                pool.query("SELECT DISTINCT room_id FROM messages WHERE reciever_id='" + reciever_id + "' AND status=0").then(total_chats => {
                    console.log("total conversations =" + total_chats.length);

                    io.in(room).emit('chat-message', {
                        message: message,
                        name: userID,
                        conversations: total_chats.length
                    })
                }).catch(err => {
                    console.log(err);

                })
            }).catch(err => {
                console.log(err);

            })

        }).catch(err => {
            console.log(err);
        })


    })
    socket.on('disconnect', () => {
        getUserRooms(socket).forEach(room => {
            socket.to(room).broadcast.emit('user-disconnected', rooms[room].users[socket.id])
            delete rooms[room].users[socket.id]
        })
    })
})

function getUserRooms(socket) {
    return Object.entries(rooms).reduce((names, [name, room]) => {
        if (room.users[socket.id] != null) names.push(name)
        return names
    }, [])
}

app.get('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


app.post('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});




// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});


module.exports = app;
