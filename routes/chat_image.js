var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');
var sizeOf = require('image-size');
const resizeImg = require('resize-img');



var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/var/www/html/img')
    },
    filename: function (req, file, cb) {
        var d = new Date();
        var time = d.getTime();
        cb(null, time + '_' + file.originalname)
    }
})

var upload = multer({
    storage: storage
});


// Upload a photo
router.post('/',verifyToken,  upload.single('image'), function (req, res, next) {

    // var image_name = req.body.image;
    var image_name = req.file.filename;
    console.log(image_name);
    var message = req.body.message;
    var sender_id = req.query.sender_id;
    var reciever_id = req.query.reciever_id;
    var room_id = req.query.room_id;


    console.log("sender_id in chat_images " + sender_id);
    console.log("reciever_id in  chat_images" + reciever_id);
    var id = 0;
    var dimensions = sizeOf('/var/www/html/img/'+image_name);
    console.log(dimensions.width, dimensions.height);
    var actual_width = dimensions.width;
    var actual_height =dimensions.height;
    var reduced_height,reduced_width;

    if(actual_height>actual_width)
    {
      console.log("height greater"); 
      reduced_width=180;
       var aspect_ratio =  actual_height / actual_width ;
       reduced_height = reduced_width * aspect_ratio
    }
    else if(actual_height<actual_width)
    {
      console.log("width greater");

      reduced_height=180;
      var aspect_ratio =  actual_width / actual_height ;
      reduced_width = reduced_height * aspect_ratio
    }
    else if(actual_height==actual_width)
    {
      console.log("width and height is equal");
      reduced_height=180;
      reduced_width = 180;
    }
 
    (async () => {
      const image = await resizeImg(fs.readFileSync('/var/www/html/img/'+image_name), {
        
          width: reduced_width,
          height: reduced_height
      });
   console.log("photo="+image_name);
   
      fs.writeFileSync('/var/www/html/chat_images/'+image_name, image);
       // delete file named 
       fs.unlink('/var/www/html/img/' + image_name, function (err) {
        if (err) throw err;
        // if no error, file has been deleted successfully
        console.log('File deleted!');
    });
  })();

    pool.query("INSERT INTO messages value(?,?,?,?,?,?)", [reciever_id, message, id, sender_id, room_id,0,image_name] )
        .then((rows) => {
            res.json({
                status: "OK"
            });
        })
        .catch(err => {
            //handle error
            console.log(err);
            res.json({
                status: "NOT Inserted"
            });
            // conn.end();
        })

});

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;

