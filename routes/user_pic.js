var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

//GET PIC for profile
router.get('/', verifyToken, function (req, res) {

    pool.query("select profile_photo_path,username,country from users where id= '" + req.query.user_id + "' ")
        .then((rows3) => {
            if (rows3[0].profile_photo_path == null || rows3[0].profile_photo_path == "") {
                console.log("image not exist");
            } else {
                const path = '/compressed/' + rows3[0].profile_photo_path;
                rows3[0].profile_photo = path;
            }
            pool.query("SELECT DISTINCT room_id FROM messages WHERE reciever_id='" + req.query.user_id + "' AND status=0").then(total_chats => {
                console.log("total conversations =" + total_chats.length);
                res.contentType('text/plain');
                res.send({
                    status: "OK",
                    username: rows3[0].username,
                    profile_photo: rows3[0].profile_photo,
                    state: rows3[0].country,
                    conversations: total_chats.length
                });
            }).catch(err => {
                console.log(err);
            })
        })
        .catch(err => {
            console.log(err);
        })
})

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
