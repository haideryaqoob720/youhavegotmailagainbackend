var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');
var sizeOf = require('image-size');
const resizeImg = require('resize-img');
require('dotenv').config();

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/var/www/html/img')
    },
    filename: function (req, file, cb) {
        var d = new Date();
        var time = d.getTime();
        cb(null, time + '_' + file.originalname)
    }
})

var upload = multer({
    storage: storage
});

// Create profile
router.post('/',verifyToken, upload.single('profile_photo'), function (req, res, next) {
    var profile_photo_path = req.file;
    console.log(profile_photo_path);
    if (profile_photo_path == undefined) {
        profile_photo_path = "";
        console.log("File Name= " + profile_photo_path);
    } else {
        profile_photo_path = req.file.filename;
        console.log("File Name =" + profile_photo_path);

        var dimensions = sizeOf('/var/www/html/img/' + profile_photo_path);
        console.log(dimensions.width, dimensions.height);
        var actual_width = dimensions.width;
        var actual_height = dimensions.height;
        var reduced_height, reduced_width;

        if (actual_height > actual_width) {
            console.log("height greater");
            reduced_width = 180;
            var aspect_ratio = actual_height / actual_width;
            reduced_height = reduced_width * aspect_ratio
        } else if (actual_height < actual_width) {
            console.log("width greater");

            reduced_height = 180;
            var aspect_ratio = actual_width / actual_height;
            reduced_height = reduced_height * aspect_ratio
        }
        else if(actual_height == actual_width)
        {
            reduced_height = 180;
            reduced_width = 180;
        }

        (async () => {
            const image = await resizeImg(fs.readFileSync('/var/www/html/img/' + profile_photo_path), {

                width: reduced_width,
                height: reduced_height
            });
            console.log("photo=" + profile_photo_path);
            fs.writeFileSync('/var/www/html/compressed/' + profile_photo_path, image);

            // delete file named 
            fs.unlink('/var/www/html/img/' + profile_photo_path, function (err) {
                if (err) throw err;
                // if no error, file has been deleted successfully
                console.log('File deleted!');
            });

        })();

    }
    var relationship_status = req.body.relationship_status;
    var hobbies = req.body.hobbies;
    var bio = req.body.bio;
    console.log("bio", bio);
    var work_experience = req.body.work_experience;
    // var lang = languages.toString();
    var id = req.query.id;
    let sql = `UPDATE users
           SET profile_photo_path = ?,
           relationship_status = ?,
           hobbies = ?,
           bio=?, 
           work_experience=?
           WHERE id = ?`;
    let data = [profile_photo_path, relationship_status, hobbies, bio, work_experience, id];
    pool.query(sql, data)
        .then((rows) => {
            res.json({
                status: "OK"
            });
        })
        .catch(err => {
            console.log("Error =" + err);
        })
});

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
