var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');
var flag=1;
var {
    Validator
} = require('node-input-validator');
require('dotenv').config();

// Sign up
router.post('/', function (req, res, next) {
    const v = new Validator(req.body, {
        username:'required|minLength:4|regex:^[a-zA-Z0-9_\.\-]+$',
        // email: 'required|email',
        password: 'required|minLength:8|regex:[a-zA-Z0-9]',
        country: 'required',
        gender :'required',
        // day: 'required',
        month: 'required',
        year: 'required'
      });
      v.check().then((matched) => {
        if (!matched) {
          res.status(422).send(v.errors);
           flag = 0;
           console.log("flag ="+flag);
        }
        if(flag == 1){
            console.log("flag in if="+flag);
    
            var username = req.body.username;
            var email = "";
            var password = req.body.password;
            var gender = req.body.gender;
            // var location = req.body.location;
            var day = "";
            var month = req.body.month;
            var year = req.body.year;
            var relationship_status = '';
            var hobbies = '';
            var country = req.body.country;
            var bio = '';
            var work_experience = '';
            var profile_photo_path = '';
            var id=0;
            // pool.getConnection()
            //     .then(conn => {
                    pool.query("select * from users where username='" + username + "'").then((rows => {
                        if (rows.length > 0) {
                            res.json({
                                message: "User Already Exist With Same username"
                            })
                        } else {
                            pool.query("INSERT INTO users value (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)", [username,id, "", day, month, year, email, password, profile_photo_path, relationship_status, hobbies, country, bio, work_experience,"online",gender])
                                .then((rows) => {
                                    const access_token = jwt.sign({
                                        username: username
                                    }, process.env.ACCESS_TOKEN_SECRET, {
                                        expiresIn: process.env.JWT_EXPIRES_IN
                                    })
                                // req.session.email = email;
                                pool.query("select * from users where username= '" + req.body.username + "' && password='" + req.body.password + "'  ").then(rows=>{
                                    console.log("id during signup "+rows[0].id);
                                    res.json({
                                        status: "OK",
                                        id:rows[0].id,
                                        access_token: access_token
                                    });
                                }).catch(err=>{
                                    console.log(err);
                                })
                                    
                                })
                                .catch(err => {
                                    //handle error
                                    console.log(err);
                                    // conn.end();
                                })
                        }
        
                    })).catch(err => {
                        res.json({
                            message:"Error in query"
                        });
                    })
          }
        
      });
     
});

module.exports = router;

