var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

// GET all block user
router.get('/',verifyToken, async function (req, res) {
    var rows = await pool.query("select blocked from blocks where blocker='" + req.query.user_id + "'");
    var len = rows.length;
    if(len==0){
        console.log(req.query.user_id+" Not Block Any User");
        res.send("Empty");
    }
    else{
    var i = 0;
    console.log("User id=" + rows[0].blocked);
    console.log("length=" + len);
    const dt = new Date();
    var current_year = dt.getFullYear();
    var arr = [];
    for (i; i < len; i++) {
        console.log("Excutution for= " + rows[i].blocked);
        var rows2 = await pool.query("select username,id,year,profile_photo_path,country from users where id='" + rows[i].blocked + "'");
        console.log("username =" + rows2[0].username);
        rows2[0].age = current_year - rows2[0].year;
        console.log("i=" + i);
        console.log("age=" + rows2[0].age);
        if (rows2[0].profile_photo_path == null || rows2[0].profile_photo_path == "") {
            console.log("image not exist");
        } else {
            const path = '/compressed/' + rows2[0].profile_photo_path;
            // const bitmap = fs.readFileSync(path);
            // var image = bitmap.toString('base64');
            // rows2[0].profile_photo = "data:image/png;base64," + image;
            rows2[0].profile_photo = path;

        }
        console.log(rows2[0]);
        arr.push(rows2[0])
    }
    res.contentType('text/plain');
    res.send(arr);
}
});


function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
