var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

//GET PIC for profile
router.get('/', verifyToken, function (req, res) {

    pool.query("SELECT DISTINCT room_id FROM messages WHERE reciever_id='" + req.query.user_id + "' AND status=0").then(total_chats => {
            console.log("total conversations =" + total_chats.length);
            res.contentType('text/plain');
            res.send({
                status: "OK",
                conversations: total_chats.length
            });
        })
        .catch(err => {
            console.log(err);
        })
})

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;
