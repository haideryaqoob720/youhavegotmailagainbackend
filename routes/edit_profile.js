var express = require('express');
var router = express.Router();
var pool = require('../db')
var multer = require('multer')
var jwt = require('jsonwebtoken');
var fs = require('fs');
var sizeOf = require('image-size');
const resizeImg = require('resize-img');

var {
    Validator
} = require('node-input-validator');
require('dotenv').config();
// var session = require('express-session');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/var/www/html/img')
    },
    filename: function (req, file, cb) {
        var d = new Date();
        var time = d.getTime();
        cb(null, time + '_' + file.originalname)
    }
})

var upload = multer({
    storage: storage
});


// edit profile
router.post('/', verifyToken, upload.single('profile_photo'), function (req, res, next) {
    // var username = req.body.username;

    var profile_photo_path = req.file;
    console.log(profile_photo_path);
    if (profile_photo_path == undefined) {
        profile_photo_path = "";
        console.log("File Name= " + profile_photo_path);
    } else {
        profile_photo_path = req.file.filename;
        console.log("File Name =" + profile_photo_path);

        var dimensions = sizeOf('/var/www/html/img/' + profile_photo_path);
        console.log(dimensions.width, dimensions.height);
        var actual_width = dimensions.width;
        var actual_height = dimensions.height;
        var reduced_height, reduced_width;

        if (actual_height > actual_width) {
            console.log("height greater");
            reduced_width = 180;
            var aspect_ratio = actual_height / actual_width;
            reduced_height = reduced_width * aspect_ratio
        } else if (actual_height < actual_width) {
            console.log("width greater");

            reduced_height = 180;
            var aspect_ratio = actual_width / actual_height;
            reduced_height = reduced_height * aspect_ratio
        }

        (async () => {
            const image = await resizeImg(fs.readFileSync('/var/www/html/img/' + profile_photo_path), {

                width: reduced_width,
                height: reduced_height
            });
            console.log("photo=" + profile_photo_path);

            fs.writeFileSync('/var/www/html/compressed/' + profile_photo_path, image);
        })();
    }
    var location = req.body.location;
    // var day = req.body.day;
    var month = req.body.month;
    var year = req.body.year;
    var bio = req.body.bio;
    var work_experience = req.body.work_experience;
    var relationship_status = req.body.relationship_status;
    var hobbies = req.body.hobbies;
    var country = req.body.country;
    var id = req.query.id;

    pool.query("UPDATE users SET year = '" + year + "',month = '" + month + "',location = '" + location + "',profile_photo_path = '" + profile_photo_path + "',relationship_status = '" + relationship_status + "',hobbies = '" + hobbies + "',country = '" + country + "',bio = '" + bio + "',work_experience = '" + work_experience + "' WHERE id = '" + id + "' ")
        .then((rows) => {
            res.json({
                status: "OK"
            });
        })
        .catch(err => {
            //handle error
            console.log("Error =" + err);
            // conn.end();
        })
});

function verifyToken(req, res, next) {
    console.log("varify  " + req);

    // Get auth header value
    const bearerHeader = req.headers['authorization'];
    // Check if bearer is undefined
    if (typeof bearerHeader !== 'undefined') {
        // Split at the space
        const bearer = bearerHeader.split(' ');
        // Get token from array
        const bearerToken = bearer[1];
        // Set the token
        req.token = bearerToken;
        jwt.verify(req.token, process.env.ACCESS_TOKEN_SECRET, (err, authData) => {
            if (err) {
                console.log("forbidden inner");
                return res.sendStatus(403);
            }
            // req.user = user;
            else {
                next();
            }
        });

    } else {
        // Forbidden
        console.log("forbidden outer");
        res.sendStatus(403);
    }
}

module.exports = router;

