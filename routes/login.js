var express = require('express');
var router = express.Router();
var pool = require('../db')
var jwt = require('jsonwebtoken');

var {
    Validator
} = require('node-input-validator');
require('dotenv').config();

// Login
router.post('/', function (req, res, next) {
    const v = new Validator(req.body, {
        username: 'required',
        password: 'required'
    });

    v.check().then((matched) => {
        if (!matched) {
            res.status(422).send(v.errors);
        }
    });
    var username = req.body.username;
    var password = req.body.password;
    var obj;

    pool.query("select * from users where username= '" + req.body.username + "' && password='" + req.body.password + "'  ")
        .then(async (rows) => {
            if (rows.length == 1) {
                const access_token = jwt.sign({
                    username: username
                }, process.env.ACCESS_TOKEN_SECRET, {
                    expiresIn: process.env.JWT_EXPIRES_IN
                })
                var profile_status = "online"
                console.log("id during login " + rows[0].id);
                const rows2 = await pool.query("UPDATE users SET status = '" + profile_status + "' WHERE username = '" + username + "' AND password = '" + password + "'  ");
                res.contentType('text/plain');
                res.send({
                    status: "OK",
                    id: rows[0].id,
                    token: access_token
                });
            } else {
                res.sendStatus(404);
            }
        })
        .catch(err => {
            //handle error
            console.log(err);
            // conn.end();
        })
});

router.get('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


router.post('/*', function (req, res) {
    res.status(404).send('Page Not Exist');
});


module.exports = router;
