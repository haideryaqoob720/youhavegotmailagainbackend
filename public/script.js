const socket = io()
const messageContainer = document.getElementById('message-container1')
const roomContainer = document.getElementById('room-container1')
const messageForm = document.getElementById('send-container1')
const messageInput = document.getElementById('message-input1')

if (messageForm != null) {
  // const name = prompt('What is your name?')
  // appendMessage('You joined')
  console.log("in if statement roomName="+roomName);
  console.log("in if statement userID="+userID);

  socket.emit('new-user', roomName, userID)


  messageForm.addEventListener('submit', e => {
    e.preventDefault()
    const message = messageInput.value
    // appendMessage(`You: ${message}`)
    socket.emit('send-chat-message', roomName,userID, message)
    messageInput.value = ''
  })
}

// socket.on('room-created', room => {
//   const roomElement = document.createElement('div')
//   roomElement.innerText = room
//   const roomLink = document.createElement('a')
//   roomLink.href = `/${room}`
//   roomLink.innerText = 'join'
//   roomContainer.append(roomElement)
//   roomContainer.append(roomLink)
// })

socket.on('chat-message', data => {
  appendMessage(`${data.name}: ${data.message}`)
})

socket.on('user-connected', name => {
  // appendMessage(`${name} connected`)
  console.log(name+" connected");
  
})

socket.on('user-disconnected', name => {
  appendMessage(`${name} disconnected`)
})

function appendMessage(message) {
  const messageElement = document.createElement('div')
  messageElement.innerText = message
  messageContainer.append(messageElement)
}